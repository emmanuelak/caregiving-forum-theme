# Caregiving forum theme

Forum customization Snippets

# Generating your icons file

```
git config core.hooksPath .githooks
```

push your code

# Adding custom icons

- Add file to assets/svg-icons
- `npm install -g svg-sprite-generator`
- `svg-sprite-generate -d assets/svg-icons -o assets/icons-sprite.svg`
- in a script tag `api.replaceIcon('bars', 'mytheme-icon-bars');`

